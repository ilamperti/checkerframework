[![pipeline status](https://gitlab.com/ilamperti/checkerframework/badges/master/pipeline.svg)](https://gitlab.com/ilamperti/checkerframework/-/pipelines/latest)

Questa repo contiene un esempio di utilizzo del [Checker Framework][1] alla versione **3.14.0**.
Può essere eseguito con **Java8** o **Java11** e qualsiasi versione di Gradle fino alla **7.0.2**.

# Annotations subproject
Questo progetto contiene le annotazioni custom da usare in altri progetti. Queste devono essere in un progetto esterno in quanto in fase di compilazione i file class **devono** già essere a disposizione.
Sono presenti annotazioni custom per:

* Fenum: annotazione semplice per specializzare l'annotazione Fenum

* Units: estensione alle annotazioni delle unità di misura per gestire Newton

* Numbers:  annotazioni per gestire numeri

## Test
Il progetto contiene test relativi all'utilizzo delle annotazioni che gestiscono il locking dei file e per i numeri.

# Presentation subproject

Questo progetto contiene degli esempi di utilizzo per alcuni checker popolari:

* Aliasing: esempi per testare l'utilizzo di `org.checkerframework.common.aliasing.AliasingChecker`
* Fenum: esempi per testare l'utilizzo di `org.checkerframework.checker.fenum.FenumChecker`
* Format: esempi per testare l'utilizzo di `org.checkerframework.checker.formatter.FormatterChecker` e `org.checkerframework.checker.i18nformatter.I18nFormatterChecker`
* Nullness: esempi per testare l'utilizzo di `org.checkerframework.checker.nullness.NullnessChecker`
* Units: esempi per testare l'utilizzo di `org.checkerframework.checker.units.UnitsChecker`

[1]: https://checkerframework.org/