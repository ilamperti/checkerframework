package tests;

import java.io.File;
import java.util.List;
import org.checkerframework.framework.test.CheckerFrameworkPerDirectoryTest;
import org.junit.runners.Parameterized;

public class LockTest extends CheckerFrameworkPerDirectoryTest {

  private static final String TEST_DATA_SUBDIR_NAME = "lock";

  public LockTest(List<File> testFiles) {
    super(
        testFiles,
        org.checkerframework.checker.lock.LockChecker.class,
        TEST_DATA_SUBDIR_NAME,
        "-Anomsgtext", // don't print error text, just the key. Used to compare against expected val
        "-nowarn",
        "-Astubs=stubs");
  }

  @Parameterized.Parameters
  public static String[] getTestDirs() {
    return new String[] {TEST_DATA_SUBDIR_NAME};
  }
}
