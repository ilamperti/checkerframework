package tests;

import it.unimi.vec.numbers.FullNumbersChecker;
import java.io.File;
import java.util.List;
import org.checkerframework.framework.test.CheckerFrameworkPerDirectoryTest;
import org.junit.runners.Parameterized;

public class NumbersTest extends CheckerFrameworkPerDirectoryTest {

  private static final String TEST_DATA_SUBDIR_NAME = "numbers";

  public NumbersTest(List<File> testFiles) {
    super(
        testFiles,
        FullNumbersChecker.class,
        TEST_DATA_SUBDIR_NAME,
        "-Anomsgtext", // don't print error text, just the key. Used to compare against expected val
        "-nowarn",
        "-Astubs=stubs");
  }

  @Parameterized.Parameters
  public static String[] getTestDirs() {
    return new String[] {TEST_DATA_SUBDIR_NAME};
  }
}
