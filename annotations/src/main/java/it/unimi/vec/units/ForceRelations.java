package it.unimi.vec.units;

import it.unimi.vec.units.qual.N;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.util.Elements;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.units.UnitsRelations;
import org.checkerframework.checker.units.UnitsRelationsTools;
import org.checkerframework.checker.units.qual.Prefix;
import org.checkerframework.checker.units.qual.g;
import org.checkerframework.checker.units.qual.mPERs2;
import org.checkerframework.framework.type.AnnotatedTypeMirror;

/** Relations among units of force. */
public class ForceRelations implements UnitsRelations {

  protected AnnotationMirror force, mass, acceleration;
  protected Elements elements;

  public UnitsRelations init(ProcessingEnvironment env) {
    elements = env.getElementUtils();

    // create Annotation Mirrors, each representing a particular Unit's Annotation
    force = UnitsRelationsTools.buildAnnoMirrorWithDefaultPrefix(env, N.class);
    mass = UnitsRelationsTools.buildAnnoMirrorWithSpecificPrefix(env, g.class, Prefix.kilo);
    acceleration = UnitsRelationsTools.buildAnnoMirrorWithDefaultPrefix(env, mPERs2.class);

    return this;
  }

  public @Nullable AnnotationMirror multiplication(
      AnnotatedTypeMirror lht, AnnotatedTypeMirror rht) {
    if (UnitsRelationsTools.hasSpecificUnit(lht, mass)
        && UnitsRelationsTools.hasSpecificUnit(rht, acceleration)) {
      return force;
    } else if (UnitsRelationsTools.hasSpecificUnit(lht, acceleration)
        && UnitsRelationsTools.hasSpecificUnit(rht, mass)) {
      return force;
    }
    return null;
  }

  @Override
  public @Nullable AnnotationMirror division(AnnotatedTypeMirror lht, AnnotatedTypeMirror rht) {
    return null;
  }
}
