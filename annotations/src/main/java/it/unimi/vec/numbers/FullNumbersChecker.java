package it.unimi.vec.numbers;

import java.util.Arrays;
import java.util.Collection;
import org.checkerframework.checker.nullness.NullnessChecker;
import org.checkerframework.framework.source.AggregateChecker;
import org.checkerframework.framework.source.SourceChecker;

public class FullNumbersChecker extends AggregateChecker {
  protected Collection<Class<? extends SourceChecker>> getSupportedCheckers() {
    return Arrays.asList(NullnessChecker.class, NumbersChecker.class);
  }
}
