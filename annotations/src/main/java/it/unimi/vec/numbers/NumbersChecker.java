package it.unimi.vec.numbers;

import java.util.LinkedHashSet;
import org.checkerframework.common.basetype.BaseTypeChecker;

public class NumbersChecker extends BaseTypeChecker {
  // to add dependencies to other checkers
  @Override
  protected LinkedHashSet<Class<? extends BaseTypeChecker>> getImmediateSubcheckerClasses() {
    return super.getImmediateSubcheckerClasses();
  }
}
