package it.unimi.vec.numbers.qual;

import java.lang.annotation.*;
import org.checkerframework.framework.qual.SubtypeOf;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE_USE, ElementType.TYPE_PARAMETER})
@SubtypeOf({Number.class})
public @interface Odd {}
