package it.unimi.vec.numbers.util;

import it.unimi.vec.numbers.qual.Even;
import it.unimi.vec.numbers.qual.Number;
import it.unimi.vec.numbers.qual.Odd;
import it.unimi.vec.numbers.qual.Percentage;
import org.checkerframework.dataflow.qual.Pure;
import org.checkerframework.framework.qual.EnsuresQualifierIf;
import org.checkerframework.framework.qual.RequiresQualifier;

public class NumbersUtil {
  private NumbersUtil() {
    throw new Error("do not instantiate");
  }

  @Pure
  @EnsuresQualifierIf(result = true, expression = "#1", qualifier = Percentage.class)
  public static boolean isPercentage(final int num) {
    return num >= 0 && num <= 100;
  }

  @Pure
  @EnsuresQualifierIf(result = true, expression = "#1", qualifier = Odd.class)
  @EnsuresQualifierIf(result = false, expression = "#1", qualifier = Even.class)
  public static boolean isOdd(final int num) {
    return num % 2 == 1;
  }

  @Pure
  @RequiresQualifier(expression = "#1", qualifier = Even.class)
  public static @Number int half(@Number int number) {
    return number / 2;
  }
}
