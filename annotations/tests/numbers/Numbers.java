package it.unimi.vec;

import it.unimi.vec.numbers.qual.*;
import org.checkerframework.framework.qual.RequiresQualifier;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.MonotonicNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import static it.unimi.vec.numbers.util.NumbersUtil.*;

public class Numbers {

    private @MonotonicNonNull Integer x;

    public Numbers(int x) {
        this.x = x;
    }

    @EnsuresNonNull("this.x")
    public void isNotNull() {
        if (x == null) {
            x = 0;
        }
    }

    public void testNull() {
        // :: error: (dereference.of.nullable)
        System.out.println(x.toString());
        isNotNull();
        System.out.println(x.toString());
    }

    public void testNumber() {
        int number = 10;
        if (isPercentage(number)) {
            @Percentage int num = number;
            System.out.println("Current percentage: " + num);
        } else {
            System.out.println("Error percentage");
        }
    }

    public void wrongTestNumber() {
        int number = 120;
        if (!isPercentage(number)) {
            // :: error: (assignment)
            @Percentage int num = number;
            System.out.println("Current percentage: " + num);
        } else {
            System.out.println("Error percentage");
        }
    }

    public void testOddEven() {
        int number = 10;
        if (isOdd(number)) {
            @Odd int num = number;
            System.out.println("Odd number: " + num);
        } else {
            @Even int num = number;
            System.out.println("Even number: " + num);
        }
    }

    public void wrongTestOddEven() {
        int number = 10;
        if (isOdd(number)) {
            @Odd int num = number;
            System.out.println("Odd number: " + num);
        } else {
            // :: error: (assignment)
            @Odd int num = number;
            System.out.println("Even number: " + num);
        }
    }

    public void testHalf() {
        int number = 20;
        if (!isOdd(number)) {
            number = half(number);
            System.out.println(number);
            // :: error: (contracts.precondition)
            number = half(number);
        } else {
            // :: error: (contracts.precondition)
            number = half(number);
        }
        System.out.println(number);
    }

}
