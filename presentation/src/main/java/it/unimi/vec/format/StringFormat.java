package it.unimi.vec.format;

public class StringFormat {

  public static void main(String[] args) {
    String.format("%y", 7);
    String.format("%d", "a string");
    String.format("%d %s", 7);
    String.format("%d", 7, 3);
  }
}
