package it.unimi.vec.format;

import java.util.Date;

public class MessageFormat {
  public static void main(String[] args) {
    java.text.MessageFormat.format("{0} {1}", 3.1415);
    java.text.MessageFormat.format("{0, time}", "my string");
    java.text.MessageFormat.format("{0, thyme}", new Date());
    java.text.MessageFormat.format("{0", new Date());
    java.text.MessageFormat.format("{0.2, time}", new Date());
    java.text.MessageFormat.format("{0, number, #.#.#}", 3.1415);
  }
}
