package it.unimi.vec.units;

import it.unimi.vec.units.qual.Force;
import it.unimi.vec.units.qual.N;
import org.checkerframework.checker.units.qual.*;
import org.checkerframework.checker.units.util.UnitsTools;

public class Newton {

  public static @kmPERh double linearMotion(@km double space, @h double time) {
    return space * time * UnitsTools.kmPERh;
  }

  public static @N double getForce(@kg double mass, @mPERs2 double acceleration) {
    // ForceRelations will cast it to N
    return mass * acceleration;
  }

  public static void main(String[] args) {
    @km double space = 100 * UnitsTools.km;
    @h double time = UnitsTools.h;
    @Speed double speed = linearMotion(space, time);
    System.out.println(speed);

    @kg double mass = UnitsTools.kg;
    @mPERs2 double acceleration = 9.8 * UnitsTools.mPERs2;
    @Force double force = getForce(mass, acceleration);
    System.out.println(force);
  }
}
