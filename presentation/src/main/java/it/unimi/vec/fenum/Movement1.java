package it.unimi.vec.fenum;

import org.checkerframework.checker.fenum.qual.Fenum;

// it is needed to suppress when initializing
@SuppressWarnings("fenum:assignment")
public class Movement1 {
  public static final @Fenum("Direction") int UP = 0;
  public static final @Fenum("Direction") int DOWN = 1;
  public static final @Fenum("Direction") int LEFT = 2;
  public static final @Fenum("Direction") int RIGHT = 3;

  public final @Fenum("Direction") int direction;

  public Movement1(@Fenum("Direction") int direction) {
    this.direction = direction;
  }

  public static void main(String[] args) {
    Movement1 m1 = new Movement1(Movement1.UP);
    Movement1 m2 = new Movement1(0);
  }
}
