package it.unimi.vec.fenum;

import it.unimi.vec.fenum.qual.Direction;

// it is needed to suppress when initializing
@SuppressWarnings("fenum:assignment")
public class Movement2 {
  public static final @Direction int UP = 0;
  public static final @Direction int DOWN = 1;
  public static final @Direction int LEFT = 2;
  public static final @Direction int RIGHT = 3;

  public final @Direction int direction;

  public Movement2(@Direction int direction) {
    this.direction = direction;
  }

  public static void main(String[] args) {
    Movement2 m1 = new Movement2(Movement2.UP);
    Movement2 m2 = new Movement2(0);
  }
}
