package it.unimi.vec.nullness;

import org.checkerframework.checker.nullness.qual.MonotonicNonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.dataflow.qual.SideEffectFree;

public class SideEffects {

  @Nullable Object field;
  @MonotonicNonNull Object field2;

  void computeValue() {
    field = null;
  }

  @SideEffectFree
  void computeValue2() {}

  void test() {
    if (field != null) {
      computeValue(); // may change the value of field
      System.out.println(field.toString());
    }
    if (field != null) {
      computeValue2();
      System.out.println(field.toString());
    }

    if (field2 != null) {
      computeValue();
      System.out.println(field2.toString());
    }
    if (field2 != null) {
      computeValue2();
      System.out.println(field2.toString());
    }
  }
}
