package it.unimi.vec.nullness;

import java.util.HashMap;
import java.util.Map;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class Example {

  public static void main(String[] args) {
    @NonNull Object o = null;

    System.out.println(o.toString());

    Map<String, @Nullable String> map = new HashMap<>();
    map.put("key", null);
    System.out.println(map.get("key").toString());
  }
}
