package it.unimi.vec.nullness;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SubClass extends BaseClass {

  public SubClass() {
    super("");
  }

  @Override
  public @NonNull String prepend(@NonNull String string) {
    return string.concat(this.str);
  }

  public static void main(String[] args) {
    BaseClass base = new SubClass();
    System.out.println(base.prepend(""));
    System.out.println(base.prepend(null));
  }
}
