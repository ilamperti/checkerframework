package it.unimi.vec.nullness;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public abstract class BaseClass {

  protected @NonNull String str;

  public BaseClass(@NonNull String string) {
    str = string;
  }

  public abstract @NonNull String prepend(@Nullable String string);
}
