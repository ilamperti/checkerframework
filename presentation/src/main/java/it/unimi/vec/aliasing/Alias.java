package it.unimi.vec.aliasing;

import org.checkerframework.common.aliasing.qual.NonLeaked;
import org.checkerframework.common.aliasing.qual.Unique;

public class Alias {
  public static String makeCopy(String value) {
    return value;
  }

  public static String makeCopy2(@NonLeaked String value) {
    return value;
  }

  public static void main(String[] args) {
    @Unique String value = "ciao";
    // cannot copy value. It is unique
    String copy = value;
    System.out.println(copy);
    // cannot copy through method
    copy = makeCopy(value);
    System.out.println(copy);
    // can copy because the parameter is annotated with NonLeaker
    copy = makeCopy2(value);
    System.out.println(copy);
    // can copy value if needed
    copy = String.join("", value.split(""));
    System.out.println(copy);
  }
}
